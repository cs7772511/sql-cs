﻿namespace LB09
{
    internal class Note
    {
        public Note(object id, object title, object content, object createdAt, object lastUpdatedAt)
        {
            ID = (int)id;
            Title = (string)title;
            Content = (string)content;
            CreatedAt = (DateTime)createdAt;
            LastUpdatedAt = (DateTime)lastUpdatedAt;
        }
        public Note(string title, string content)
        {
            Title = title;
            Content = content;
            CreatedAt = DateTime.Now;
            LastUpdatedAt = DateTime.Now;
        }

        public override string ToString()
        {
            return $"{Title}";
        }
        public int ID { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime LastUpdatedAt { get; set; }
    }
}

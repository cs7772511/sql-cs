﻿namespace LB09
{
    partial class NoteForm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            tbContent = new TextBox();
            btnNoviZapis = new Button();
            btnPohrani = new Button();
            lbTitles = new ListBox();
            SuspendLayout();
            // 
            // tbContent
            // 
            tbContent.Location = new Point(264, 22);
            tbContent.Multiline = true;
            tbContent.Name = "tbContent";
            tbContent.Size = new Size(508, 349);
            tbContent.TabIndex = 1;
            // 
            // btnNoviZapis
            // 
            btnNoviZapis.Location = new Point(12, 389);
            btnNoviZapis.Name = "btnNoviZapis";
            btnNoviZapis.Size = new Size(202, 37);
            btnNoviZapis.TabIndex = 2;
            btnNoviZapis.Text = "Novi zapis";
            btnNoviZapis.UseVisualStyleBackColor = true;
            btnNoviZapis.Click += btnNoviZapis_Click;
            // 
            // btnPohrani
            // 
            btnPohrani.Location = new Point(560, 389);
            btnPohrani.Name = "btnPohrani";
            btnPohrani.Size = new Size(202, 37);
            btnPohrani.TabIndex = 3;
            btnPohrani.Text = "Pohrani";
            btnPohrani.UseVisualStyleBackColor = true;
            btnPohrani.Click += btnPohrani_Click;
            // 
            // lbTitles
            // 
            lbTitles.FormattingEnabled = true;
            lbTitles.ItemHeight = 15;
            lbTitles.Location = new Point(12, 22);
            lbTitles.Name = "lbTitles";
            lbTitles.Size = new Size(236, 349);
            lbTitles.TabIndex = 4;
            lbTitles.DoubleClick += lbTitles_DoubleClick;
            // 
            // NoteForm
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(800, 450);
            Controls.Add(lbTitles);
            Controls.Add(btnPohrani);
            Controls.Add(btnNoviZapis);
            Controls.Add(tbContent);
            Name = "NoteForm";
            Text = "Form1";
            Load += NoteForm_Load;
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion
        private TextBox tbContent;
        private Button btnNoviZapis;
        private Button btnPohrani;
        private ListBox lbTitles;
    }
}
namespace LB09
{
    public partial class NoteForm : Form
    {

        public NoteForm()
        {
            InitializeComponent();
        }
        private void NoteForm_Load(object sender, EventArgs e)
        {
            RefreshNotesList();
        }
        private void RefreshNotesList()
        {
            lbTitles.Items.Clear();
            foreach (Note note in Notes.GetNotes())
            {
                lbTitles.Items.Add(note);
            }

            if (lbTitles.Items.Count > 0)
            {
                lbTitles.SelectedIndex = 0;
            }
        }


        private void btnNoviZapis_Click(object sender, EventArgs e)
        {
            var note = new Note("Abeceda", "nema jos abecede");
            Notes.InsertNote(note);
            RefreshNotesList();
        }

        private void btnPohrani_Click(object sender, EventArgs e)
        {
            var selectedNote = (Note)lbTitles.SelectedItem;
            selectedNote.Content = tbContent.Text;
            Notes.UpdateNote(selectedNote);
            RefreshNotesList();
        }

        private void lbTitles_DoubleClick(object sender, EventArgs e)
        {
            var note = (Note)lbTitles.Items[lbTitles.SelectedIndex];
            tbContent.Text = note.Content;

        }
    }
}
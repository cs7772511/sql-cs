﻿using System.Data;
using System.Data.SqlClient;
namespace LB09
{
    internal class Notes
    {
        #region GetNotes()
        public static List<Note> GetNotes()
        {
            List<Note> notes = new List<Note>();
            string query = "SELECT * FROM Notes";

            using (SqlConnection sqlConnection = new SqlConnection(Database.ConnectionString()))
            {
                SqlDataAdapter adapter = new SqlDataAdapter(query, sqlConnection);
                DataSet ds = new DataSet();
                adapter.Fill(ds);
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    notes.Add(new Note(
                        Convert.ToInt32(row["ID"]),
                        row["Title"].ToString(),
                        row["Content"].ToString(),
                        Convert.ToDateTime(row["CreatedAt"]),
                        Convert.ToDateTime(row["UpdatedAt"])
                    ));
                }
            }
            return notes;
        }
        #endregion GetNotes()

        #region InsertNote()
        public static bool InsertNote(Note note)
        {
            string query = "INSERT INTO Notes (Title, Content) VALUES (@Title, @Content)";

            using (SqlConnection sqlConnection = new SqlConnection(Database.ConnectionString()))
            {
                using (SqlCommand command = new SqlCommand(query, sqlConnection))
                {
                    command.Parameters.AddWithValue("@Title", note.Title);
                    command.Parameters.AddWithValue("@Content", note.Content);

                    sqlConnection.Open();
                    int result = command.ExecuteNonQuery();
                    if (result < 0)
                    {
                        return false;
                    }
                }
            }
            return true;
        }
        #endregion InsertNote()
        #region UpdateNote()
        public static bool UpdateNote(Note note)
        {
            string query = "UPDATE Notes SET Title = @Title, Content = @Content, UpdatedAt = @UpdatedAt WHERE ID = @ID";

            using (SqlConnection sqlConnection = new SqlConnection(Database.ConnectionString()))
            {
                using (SqlCommand command = new SqlCommand(query, sqlConnection))
                {
                    command.Parameters.AddWithValue("@ID", note.ID);
                    command.Parameters.AddWithValue("@Title", note.Title);
                    command.Parameters.AddWithValue("@Content", note.Content);
                    command.Parameters.AddWithValue("@UpdatedAt", DateTime.Now);

                    sqlConnection.Open();
                    int result = command.ExecuteNonQuery();
                    if (result < 0)
                    {
                        return false;
                    }
                }
            }
            return true;
        }
        #endregion UpdateNote()
    }
}
